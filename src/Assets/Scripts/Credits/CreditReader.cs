using TMPro;
using UnityEngine;

public class CreditReader : MonoBehaviour
{
    public TMP_Text textBox;
    public GameObject textBlock;
    public TextAsset jsonFile;

    public float speed;
    public float lineSize;
    private float size;
    public GameObject target;
    
    void Start()
    {
        speed = Screen.height / 12;
        AudioManager.instance.Play("credits"); 
        Category creditsInJson = JsonUtility.FromJson<Category>(jsonFile.text);
        foreach (Credits credits in creditsInJson.category)
        {
            size += lineSize * 2;
            textBox.text += credits.type + ":\n\n";
            foreach (Credit credit in credits.credits)
            {
                size += lineSize;
                textBox.text += credit.name + ": " + credit.creator + "\n";
            }

            size += lineSize * 2;
            textBox.text += "\n\n";
        }

    }

    private void Update()
    {
        ScrollCredits();
    }

    private void ScrollCredits()
    {
        float scrollSpeed = speed * Time.deltaTime;
        textBlock.transform.position = Vector2.MoveTowards(new Vector2(0, textBlock.transform.position.y), new Vector2(0, target.transform.position.y), scrollSpeed);
    }
}