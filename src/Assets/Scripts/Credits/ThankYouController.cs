using UnityEngine;

public class ThankYouController : MonoBehaviour
{
    public CreditReader creditReader;

    private void OnCollisionEnter2D(Collision2D other)
    {
        if (other.gameObject.CompareTag("Target"))
        {
            creditReader.speed = 0;
        }
    }
}
