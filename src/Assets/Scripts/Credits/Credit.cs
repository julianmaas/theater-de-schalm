[System.Serializable]
public class Credit
{
    public string name;
    public string creator;
}

[System.Serializable]
public class Category
{
    public Credits[] category;
}

[System.Serializable]
public class Credits
{
    public string type;
    public Credit[] credits;
}