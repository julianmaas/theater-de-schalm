using UnityEngine;

public class NpcController : MonoBehaviour
{
    public Transform[] waypoints;
    [SerializeField] private float moveSpeed = 2f;
    private int waypointIndex = 0;
    private SpriteRenderer mySpriteRenderer;
    
    // Start is called before the first frame update
    void Start()
    {
        transform.position = waypoints[waypointIndex].transform.position;
        mySpriteRenderer = GetComponent<SpriteRenderer>();
    }
    // Update is called once per frame
    void Update()
    {
        Move();
    }

    void Move()
    {
        transform.position = Vector2.MoveTowards(transform.position, waypoints[waypointIndex].transform.position,
            moveSpeed * Time.deltaTime);

        if (transform.position == waypoints[waypointIndex].transform.position)
        {
            waypointIndex++;   
            mySpriteRenderer.flipX = !mySpriteRenderer.flipX;
        }

        if (waypointIndex == waypoints.Length)
            waypointIndex = 0;
    } 
}
