using UnityEngine;

public class SoundManager : MonoBehaviour
{
    private AudioSource asource;
    
    private void Awake()
    {
        asource = GetComponent<AudioSource>();
    }
    
    public void SetAudioSource(AudioSource source)
    {
        asource = source;
    }

    public void PlayAudio()
    {
        asource.Play();
    }
}
