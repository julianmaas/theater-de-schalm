using UnityEngine;

public class UiManager : MonoBehaviour
{

    public void FlipStatusOfElement(GameObject elementToShow)
    {
        elementToShow.SetActive(!elementToShow.activeSelf);
    }

    public void HideElement(GameObject elementToShow)
    {
        elementToShow.SetActive(false);
    }
}
