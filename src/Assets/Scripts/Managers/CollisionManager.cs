using JetBrains.Annotations;
using UnityEngine;
using UnityEngine.Events;

public class CollisionManager : MonoBehaviour
{
    [CanBeNull] public GameObject element;
    [CanBeNull] public string tagToCheck;
    public UnityEvent eventToInvoke;

    // Start is called before the first frame update

    private void Start()
    {
        if (element)
        {
            tagToCheck = element.tag;
        }
    }

    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.CompareTag(tagToCheck))
        {
            eventToInvoke.Invoke();
        }
    }


    void OnTriggerExit2D(Collider2D other)
    {
        if (other.gameObject.CompareTag(tagToCheck))
        {
            eventToInvoke.Invoke();
        }
    }

    void OnCollisionEnter2D(Collision2D other)
    {
        if (other.gameObject.CompareTag(tagToCheck))
        {
            eventToInvoke.Invoke();
        }
    }
    void OnCollisionEnter(Collision other)
    {
        if (other.gameObject.CompareTag(tagToCheck))
        {
            eventToInvoke.Invoke();
        }
    }
}
