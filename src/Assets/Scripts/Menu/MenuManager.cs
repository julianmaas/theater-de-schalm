using UnityEngine;
using UnityEngine.SceneManagement;

public class MenuManager : MonoBehaviour
{
    public void StartGame()
    {
        PlayerPrefs.SetInt("ChangingCourse", 0);
        PlayerPrefs.SetInt("Fire", 0);
        PlayerPrefs.SetInt("Helmsmen", 0);
        PlayerPrefs.SetInt("Storm", 0);
        PlayerPrefs.SetInt("Discovery", 0);
        PlayerPrefs.SetInt("Climbing", 0);
        SceneManager.LoadScene("Theatre", LoadSceneMode.Single);
    }
    
    public void Credits()
    {
        SceneManager.LoadScene("Credits", LoadSceneMode.Single);
    }
    
    public void MainMenu()
    {
        AudioManager.instance.Stop("credits");
        SceneManager.LoadScene("Menu", LoadSceneMode.Single);
    }
    
    public void QuitGame()
    {
        Application.Quit();
    }
    
}
