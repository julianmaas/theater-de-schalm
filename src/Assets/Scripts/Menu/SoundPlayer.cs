using UnityEngine;

public class SoundPlayer : MonoBehaviour
{
    public AudioSource sound;

    public void PlaySound()
    {
        sound.Play();
    }
}
