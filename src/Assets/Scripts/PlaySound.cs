using UnityEngine;

public class PlaySound : MonoBehaviour
{
    public string soundName;
    public void PlayClickSound()
    {
        AudioManager.instance.Play(soundName);
    }
}
