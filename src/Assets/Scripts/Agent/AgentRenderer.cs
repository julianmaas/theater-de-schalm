using UnityEngine;

[RequireComponent(typeof(SpriteRenderer))]
public class AgentRenderer : MonoBehaviour
{
    protected SpriteRenderer spriteRenderer;

    private void Awake()
    {
        spriteRenderer = GetComponent<SpriteRenderer>();
    }
    
    public void FaceDirection(Vector2 pointerinput) {
        var direction = (Vector3) pointerinput - transform.position; //cast as 3d vector and subtract player position
        var result = Vector3.Cross(Vector2.up, direction); //calculate angle between line upward from player to cursor pos, positive z means going left, negative z going right. --> flip left if going left, flip right if going right

        if (result.z > 0) {
            spriteRenderer.flipX = true;
        } else if (result.z < 0) {
            spriteRenderer.flipX = false;
        }
    }
}
