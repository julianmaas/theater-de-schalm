using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;

public class AgentInput : MonoBehaviour
{
    private Camera mainCamera;
    // [SerializeField]
    // public UnityEvent<Vector2> onMovementKeyPressed;
    [field: SerializeField] //show private variable's value in the inspector.
    public UnityEvent<Vector2> OnMovementKeyPressed { get; set; }

    [field: SerializeField] //expose to inspector
    public UnityEvent<Vector2> OnPointerPositionChange { get; set; }

    [field: SerializeField]
    public UnityEvent<Vector2> OnPointerClick { get; set; }
    
    private void Awake()
    {
        mainCamera = Camera.main; //get the main camera
    }
    
    private void Update()
    {
        GetMovementInput();
        GetPointerInput();
    }
    
    private void GetMovementInput()
    {
        OnMovementKeyPressed?.Invoke(new Vector2(Input.GetAxisRaw("Horizontal"), Input.GetAxisRaw("Vertical"))); //if someone is listening pass vector 2 value of key input to the listener
    }

    private void GetPointerInput()
    { //translate pointer/cursor to a position in the world space
        if (EventSystem.current.IsPointerOverGameObject())
            return;
        Vector3 mousePos = Input.mousePosition;
        mousePos.z = mainCamera.nearClipPlane; //fix clipping plane for main camera.
        var mouseInWorldSpace = mainCamera.ScreenToWorldPoint(mousePos);
        OnPointerPositionChange?.Invoke(mouseInWorldSpace);
        if (Input.GetMouseButtonDown(0)) OnPointerClick?.Invoke(mouseInWorldSpace);
    }

}
