using UnityEngine;

public class AgentInteractions : MonoBehaviour
{
    public GameObject buttonActivate;
    public string tagToCheck;

    // Start is called before the first frame update
    void Start()
    {
        buttonActivate.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {

    }

    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.CompareTag("TheatreChair"))
        {
            buttonActivate.SetActive(true);
        }
        if (other.gameObject.CompareTag("TheatreHallway"))
        {
            buttonActivate.SetActive(false);
        }
        if (other.gameObject.CompareTag(tagToCheck))
        {
            buttonActivate.SetActive(true);
        }
    }


    void OnTriggerExit2D(Collider2D other)
    {
        if (other.gameObject.CompareTag("TheatreChair"))
        {
            buttonActivate.SetActive(false);
        }
        if (other.gameObject.CompareTag("TheatreHallway"))
        {
            buttonActivate.SetActive(false);
        }
        if (other.gameObject.CompareTag(tagToCheck))
        {
            buttonActivate.SetActive(false);
        }
    }

    void OnCollisionEnter2D(Collision2D other)
    {
        if (other.gameObject.CompareTag(buttonActivate.tag))
        {
            buttonActivate.SetActive(true);
        }
    }
    void OnCollisionEnter(Collision other)
    {
        if (other.gameObject.CompareTag(buttonActivate.tag))
        {
            buttonActivate.SetActive(true);
        }
    }
}
