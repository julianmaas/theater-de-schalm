using UnityEngine;

[RequireComponent(typeof(Animator))]
public class AgentAnimations : MonoBehaviour
{
    //access the animator
    protected Animator agentAnimator;

    private void Awake() {
        agentAnimator = GetComponent<Animator>();
    }

    public void SetWalkAnimation(bool val) {
        agentAnimator.SetBool("Walk", val);
    }

    public void AnimatePlayer(float velocity) {
        SetWalkAnimation(velocity > 0); //pass if velocity > 0 check as an argument, 0 or 1. (evaluates to true or false)
    }
}
