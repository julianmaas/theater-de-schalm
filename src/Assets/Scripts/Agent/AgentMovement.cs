using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Events;

[RequireComponent(typeof(Rigidbody2D))]

public class AgentMovement : MonoBehaviour
{
    protected Rigidbody2D rigidBody2d; //encapsulated in agentmovement

    [field: SerializeField]
    public MovementDataSO MovementData { get; set; }


    [SerializeField]
    protected internal float currentVelocity = 3;
    protected Vector2 movementDirection;

    [field: SerializeField]
    public UnityEvent<float> OnVelocityChange { get; set; }
    
    private void Awake() {
        rigidBody2d = GetComponent<Rigidbody2D>();
    }
    
    public void MoveAgent(Vector2 movementInput)
    {
        if (targetPositionMode) return;
        if (movementInput.magnitude > 0) //convert movementinput to float value
        {
            if (Vector2.Dot(movementInput.normalized, movementDirection) < 0) //value between 1 and -1, -1 means vectors in opposite direction and 1 means in same direction
                currentVelocity = 0; //keep movement tight/fluid if the player wants to move in opposite direction --> no smoothing
            movementDirection = movementInput.normalized; //transform value into a vector, but lose magnitude information.
        }
        currentVelocity = CalculateSpeed(movementInput); //call calculate speed method.
    }
    
    private float CalculateSpeed(Vector2 movementInput) {
        if (movementInput.magnitude > 0) {
            currentVelocity += MovementData.acceleration * Time.deltaTime;
        } else {
            currentVelocity -= MovementData.deacceleration * Time.deltaTime;
        }
        return Mathf.Clamp(currentVelocity,0,MovementData.maxSpeed);
    }
    
    private void FixedUpdate() //fixed update for rigidbody2d
    {
        OnVelocityChange?.Invoke(currentVelocity);
        if (targetPositionMode)
        {
            rigidBody2d.velocity = currentVelocity * CalculateDirection(); // TODO: "Smoother" speed
            StuckCheck();
            HandleTargetPositionHistory(transform.position);
        } 
        else 
            rigidBody2d.velocity = currentVelocity * movementDirection.normalized;
    }

    #region TargetPosition
    private bool targetPositionMode = false;
    private Vector2 targetPosition;
    private LinkedList<Vector2> positionHistory = new LinkedList<Vector2>();
    private DateTime lastCollisionEnter;

    public void MoveAgentToPosition(Vector2 movementInput)
    {
        lastCollisionEnter = default;
        targetPositionMode = true;
        targetPosition = movementInput;
        currentVelocity = MovementData.maxSpeed;
    }

    private void StuckCheck()
    {
        if (lastCollisionEnter != default && (DateTime.Now - lastCollisionEnter).TotalMilliseconds >= 100)
        {
            StopMovement();
        }
    }

    public void StopMovement()
    {
        targetPositionMode = false;
        currentVelocity = 0.0f;
    }
    
    private void HandleTargetPositionHistory(Vector2 position)
    {
        positionHistory.AddLast(position);
        if (positionHistory.Count > 3)
        {
            positionHistory.RemoveFirst();
            if (positionHistory.All(currentPosition => Nearby(targetPosition, currentPosition)))
            {
                StopMovement();
            }
        }
    }

    private Vector2 CalculateDirection() =>
        new Vector2(targetPosition.x - transform.position.x , targetPosition.y - transform.position.y).normalized;

    private bool Nearby(Vector2 target, Vector2 current) => 
        Math.Abs(target.x - current.x) <= 0.1 && Math.Abs(target.y - current.y) <= 0.1;

    
    private void OnCollisionEnter2D(Collision2D other)
    {
        lastCollisionEnter = DateTime.Now;
    }

    private void OnCollisionExit2D(Collision2D other)
    {
        lastCollisionEnter = default;
    }

    #endregion

}
