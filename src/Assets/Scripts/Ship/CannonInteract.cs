using UnityEngine;

public class CannonInteract : MonoBehaviour
{
    public string animName;
    public string animParentName;
    private Animator anim;
    // Start is called before the first frame update
    void Start()
    {
      
        anim = GetComponent<Animator>();
    }

    // Update is called once per frame
    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            gameObject.transform.parent.gameObject.GetComponent<Animator>().Play(animName);
            anim.Play(animParentName);
            Destroy(gameObject.transform.parent.gameObject, 3.0f);
            Destroy(gameObject, 3.0f);
        }
    }
    
}
