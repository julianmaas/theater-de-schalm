using UnityEngine;

public class ShipGameManager : MonoBehaviour
{
    public GameObject[] elementsToHide;
    // Start is called before the first frame update
    void Start()
    {
        if (!AudioManager.instance.GetSound("shipAmbience").source.isPlaying)
        {
            AudioManager.instance.Play("shipAmbience");   
        }
        foreach (GameObject element in elementsToHide)
        {
            element.SetActive(false);
        }
    }
}
