using UnityEngine;

[CreateAssetMenu (fileName = "ServiceLocatorConfig", menuName = "ScriptableObject/ServiceLocator", order = 1)]
public class ServiceLocator : ScriptableObject
{
    private AudioManager audioManager;
    public GameObject audioManagerPrefab;

    public AudioManager GetAudioManager()
    {
        if (audioManager == null)
        {
            GameObject audioManagerGameObject = Instantiate(audioManagerPrefab);
            DontDestroyOnLoad(audioManagerGameObject);
            audioManager = audioManagerGameObject.GetComponent<AudioManager>();
        }
        return audioManager;
    }
}
