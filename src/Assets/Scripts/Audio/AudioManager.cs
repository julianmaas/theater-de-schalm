using System;
using UnityEngine;

public class AudioManager : MonoBehaviour
{
    public Sound[] sounds;
    public static AudioManager instance;
    
    void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
        else
        {
            Destroy(gameObject);
            return;
        }
        DontDestroyOnLoad(gameObject);
    
        foreach (Sound sound in sounds)
        {
            sound.source = gameObject.AddComponent<AudioSource>();
            sound.source.clip = sound.clip;
            sound.source.volume = sound.volume;
            sound.source.loop = sound.loop;
        }
    }

    public void Play(string name)
    {
        Sound s = Array.Find(sounds, sounds => sounds.name == name);
        try
        {
            s.source.Play();
        }
        catch (Exception e)
        {
            Debug.Log(e);
            Debug.LogWarning("Can't play file " + name);
        }
    }
    

    public void Stop(string name)
    {
        Sound s = Array.Find(sounds, sounds => sounds.name == name);
        s?.source.Stop();
    }

    public void StopAll()
    {
        Array.ForEach(sounds, s => s.source.Stop());
    }

    public Sound GetSound(string name)
    {
        return Array.Find(sounds, sounds => sounds.name == name);;
    }
}