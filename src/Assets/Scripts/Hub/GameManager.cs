using UnityEngine;

public class GameManager : MonoBehaviour
{
    public GameObject buttonActivate;

    public void ShowButton()
    {
        buttonActivate.SetActive(true);
    }

    public void HideButton()
    {
        buttonActivate.SetActive(false);
    }
}
