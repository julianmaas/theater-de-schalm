using UnityEngine;

public class TheatreGameManager : MonoBehaviour
{
    [SerializeField] private Animator[] _animators;
    public string parameterToFlip;
    public bool winState;

    private void Start()
    {
        AudioManager.instance.Play("crowdAmbience");
    }

    public void StartAnimation()
    {
        AudioManager.instance.Stop("crowdAmbience");
        winState = true;
        foreach (Animator animator in _animators)
        {
            animator.SetBool(parameterToFlip, !animator.GetBool(parameterToFlip));
        }
    }
}
