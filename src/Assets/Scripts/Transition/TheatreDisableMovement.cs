﻿using UnityEngine;

public class TheatreDisableMovement : MonoBehaviour
{
    public TheatreGameManager game;
    public float xPos, yPos;
    
    private bool activated = false;
    
    public void Update()
    {
        if (activated) return;
        
        if (game.winState)
        {
            Destroy(gameObject.GetComponent<AgentInput>());
            var agentMovement = gameObject.GetComponent<AgentMovement>();
            agentMovement.currentVelocity = 0.0f;
            transform.position = new Vector2(xPos, yPos);
            activated = true;
        }
    }
}