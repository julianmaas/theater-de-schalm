using System;
using UnityEngine;

public class TreasureIsland : MonoBehaviour
{
    public GameObject sceneTransition;
    public Transform playerLocation;
    public float speed, bobInterval, bobAmount;
    
    private float passedTime = 0.0f;
    private bool bobUpwards = true;
    
    private float Speed => speed * Time.deltaTime * (0.5f + (DistanceRemaining/15.0f));
    private float DistanceRemaining => Math.Abs(playerLocation.position.x - transform.position.x);

    private bool collided = false;
    
    void Update()
    {
        if (collided) return;
        
        transform.position = Vector2.MoveTowards(transform.position, 
                    playerLocation.position, Speed);
       
        passedTime += Time.deltaTime;
        if (passedTime >= bobInterval)
        {
            transform.position = bobUpwards
                ? new Vector2(transform.position.x, transform.position.y + bobAmount)
                : new Vector2(transform.position.x, transform.position.y - bobAmount);
            passedTime = 0.0f;
            bobUpwards = !bobUpwards;
        }
    }
    
    void OnTriggerEnter2D(Collider2D other)
    {
        collided = true;
        sceneTransition.GetComponent<Animator>().SetTrigger("FadeOut");
        
    }
}
