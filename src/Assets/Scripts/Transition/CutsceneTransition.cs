﻿using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.Video;

public class CutsceneTransition : MonoBehaviour
{
    public float durationInSeconds;
    public string sceneName;
    public string fileName;
    [Range(0.0f, 1.0f)]
    public float volume;

    void Start()
    {
        GetComponent<VideoPlayer>().url = System.IO.Path.Combine(Application.streamingAssetsPath, fileName + ".mp4");
        GetComponent<VideoPlayer>().Play();
        GetComponent<VideoPlayer>().SetDirectAudioVolume(0, volume);
        StartCoroutine(SwitchScene());
    }

    void OnMouseDown()
    {
        SceneManager.LoadScene(sceneName);
    }

    private IEnumerator SwitchScene()
    {
        yield return new WaitForSeconds(durationInSeconds);
        SceneManager.LoadScene(sceneName);
    }
}