﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class SwitchToTheDiscovery : MonoBehaviour
{
   void Start()
   {
       if (PlayerPrefs.GetInt("ChangingCourse") == 1 &&
           PlayerPrefs.GetInt("Storm") == 1 &&
           PlayerPrefs.GetInt("Climbing") == 1 &&
           PlayerPrefs.GetInt("Fire") == 1)
      {
          AudioManager.instance.StopAll();
          SceneManager.LoadScene("Arrival");
      }
   }
   
}
