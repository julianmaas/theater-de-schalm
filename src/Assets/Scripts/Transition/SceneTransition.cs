using JetBrains.Annotations;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneTransition : MonoBehaviour
{
    [CanBeNull] public Animator animator;
    [CanBeNull] public string sceneToLoad;
    public bool onClickTransition = false;
    
    public void SetSceneToLoad(string scene)
    {
        sceneToLoad = scene;
    }
    
    public void FadeTransition()
    {
        animator?.SetTrigger("FadeOut");
    }

    public void ChangeScene()
    {
        AudioManager.instance.StopAll();
        SceneManager.LoadScene(sceneToLoad, LoadSceneMode.Single);
    }
    
    public void ChangeToMinigame(string minigame)
    {
        SceneManager.LoadScene(minigame, LoadSceneMode.Single);
    }

    void OnMouseDown()
    {
        if (onClickTransition) ChangeScene();
    }
}
 