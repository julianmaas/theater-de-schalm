﻿using UnityEngine;

public class GameWonCheck : MonoBehaviour
{
    public string winFlag;

    public bool removeColliderKeepObject = false;
    
    // Hide if a minigame has been won
    void Start()
    {
        if (PlayerPrefs.HasKey(winFlag) && PlayerPrefs.GetInt(winFlag) == 1)
        {
            if (!removeColliderKeepObject)
                Destroy(gameObject);
            else
                Destroy(gameObject.GetComponent<BoxCollider2D>());
        }
    }
}