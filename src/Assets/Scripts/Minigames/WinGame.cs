﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class WinGame : MonoBehaviour
{
    public string winFlag;
    public string sceneName;
    public float transitionDuration = 7.0f;
    public float scale = 1.0f;
    public GameObject clickNotifier;
    
    private bool invoked = false;
    private bool animationDone = false;
    private float TransitionAmountPerSecond => scale / transitionDuration * 2;
    
    void Update()
    {
        if (!invoked || animationDone) return;
        
        if (transform.localScale.x < scale) transform.localScale = CalculateNewScale();
        else
        {
            animationDone = true;
            clickNotifier.GetComponent<AnimateClickNotifier>().StartShowing();
        }
    }
    
    public void Invoke()
    {
        PlayerPrefs.SetInt(winFlag, 1);
        invoked = true;
        AudioManager.instance.StopAll();
        AudioManager.instance.Play("victory");
    }

    private void OnMouseDown()
    {
        if (animationDone) SceneManager.LoadScene(sceneName);
    }

    private Vector2 CalculateNewScale()
    {
        float scale = transform.localScale.x + (Time.deltaTime * TransitionAmountPerSecond);
        return new Vector2(scale, scale);
    }
}