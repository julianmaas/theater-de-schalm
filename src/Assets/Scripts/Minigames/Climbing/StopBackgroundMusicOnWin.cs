﻿using UnityEngine;

public class StopBackgroundMusicOnWin : MonoBehaviour
{
    public PlayerController game;

    private bool activated;
    
    void Update()
    {
        if (!activated && game.WonState)
        {
            activated = true;
            GetComponent<AudioSource>().volume = 0.0f;
        }
    }
}
