﻿using System.Collections.Generic;
using UnityEngine;

public class Spawner : MonoBehaviour
{
    public ICollection<GameObject> spawnedObstacles = new List<GameObject>();
    public List<float> spawnedPositions = new List<float>();
    
    public GameObject[] obstacles;
    public GameObject rope;
    public GameObject leftBorder;
    public GameObject rightBorder;
    
    public float spawnInterval;
    private float timeBetweenSpawn;
    private bool leftSpawn = true;

    public int unspawnRange;
    public int countTillRangeDecrease;
    
    public bool LostState { get; set; } = false;
    
    private void Update()
    {
        if (!LostState)
            if (timeBetweenSpawn <= 0.0f && rope.transform.position.y >= -5.0f)
            {
                Spawn(unspawnRange, countTillRangeDecrease);
            }
            else timeBetweenSpawn -= Time.deltaTime;
    }

    private void Spawn(float newUnspawnRange, int count)
    {
        int obstacleNo = Random.Range(0, obstacles.Length);
        float x = Random.Range(leftSpawn ? leftBorder.transform.position.x + 1 : 0,
            leftSpawn ? 0 : rightBorder.transform.position.x - 1);
        for (int i = 0; i < spawnedPositions.Count; i++)
        {
            float asd = spawnedPositions[i];
            if (!IsSpawnValid(x, asd, newUnspawnRange))
            {
                if (count == 0)
                {
                    Spawn(newUnspawnRange - 1, countTillRangeDecrease);
                    return;
                }

                count--;
                Spawn(newUnspawnRange, count);
                return;
            }
        }
        
        GameObject obstacle = Instantiate(obstacles[obstacleNo], new Vector3(x, 11), Quaternion.identity);

        spawnedPositions.Add(obstacle.transform.position.x);
        if (spawnedPositions.Count > 2)
        {
            spawnedPositions.RemoveAt(0);
        }
        
        spawnedObstacles.Add(obstacle);
        timeBetweenSpawn = spawnInterval; leftSpawn = !leftSpawn;
    }

    private bool IsSpawnValid(float birdPos, float checkPos, float range)
    {
        float leftMax = checkPos - range;
        float rightMax = checkPos + range;

        if (birdPos > leftMax && birdPos < rightMax)
            return false;

        return true;
    }
}