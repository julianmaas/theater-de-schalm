﻿using UnityEngine;

public class Mover : MonoBehaviour
{
    public float xMovement;
    public float yMovement;
    
    public bool LostState { get; set; } = false;
    
    private void Update()
    {
        if (!LostState) transform.position = new Vector3(transform.position.x - (xMovement * 1000 * Time.deltaTime)
            , transform.position.y - (yMovement * 1000 * Time.deltaTime));
    }
}
