﻿using UnityEngine;

public class Destroyer : MonoBehaviour
{
    public float edge = -6.575f;
    
    private void Update()
    {
        if (transform.position.y <= edge)
        {
            Destroy(gameObject);
        }
    }
}