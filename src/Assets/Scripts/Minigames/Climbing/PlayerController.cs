using JetBrains.Annotations;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    public GameObject leftBorder;
    public GameObject rightBorder;
    public GameObject rope;

    public Animator[] arms;
    
    [CanBeNull] private Rigidbody2D rb;

    public GameObject gameOverButton;

    public GameObject spawner;

    public GameObject win;
    
    public bool LostState { get; set; } = false;
    public bool WonState { get; set; } = false;
    #region Events

    private void Start()
    {
        AudioManager.instance.Play("rope");
        AudioManager.instance.Play("climbingMusic");
        rb = this.GetComponent<Rigidbody2D>();
    }

    private void Update()
    {
        HandleInput();
        if (rope.transform.position.y <= -23)
        {
            if (!WonState) 
            {
                win.GetComponent<WinGame>().Invoke();
                EndPlayerChanges();
                WonState = true;
            }
            rope.transform.position = new Vector2(rope.transform.position.x, -23.0f);
        }
        if(this.gameObject.transform.localPosition.y < -20f)
            rb.constraints = RigidbodyConstraints2D.FreezePositionY;
    }
    
    private void OnTriggerEnter2D(Collider2D other)
    {
        // Lost
        if (other.CompareTag("Parrot"))
        {
            GameOver();
        }
    }
    
    #endregion
    
    #region Input
    private void HandleInput()
    {
        if (!LostState)
        {
            if (LeftInput || transform.position.x < leftBorder.transform.position.x)
            {
                transform.position = Vector2.MoveTowards(transform.position,
                    leftBorder.transform.position, 20 * Time.deltaTime);
            }
            if (RightInput || transform.position.x > rightBorder.transform.position.x)
            {
                transform.position = Vector2.MoveTowards(transform.position,
                    rightBorder.transform.position, 20 * Time.deltaTime);
            }   
        }
    }
    private bool LeftInput => Input.GetKey(KeyCode.A) || Input.GetKey(KeyCode.LeftArrow);
    private bool RightInput => Input.GetKey(KeyCode.D) || Input.GetKey(KeyCode.RightArrow);
    #endregion


    private void GameOver()
    {
        EndPlayerChanges();
        
        AudioManager.instance.Play("player_fall");

        rb.constraints = ~RigidbodyConstraints2D.FreezePositionY; 
        rb.AddForce(transform.up * 0.1f);
        LostState = true;
        gameOverButton.SetActive(true);
        rope.GetComponent<Mover>().LostState = true;
        spawner.GetComponent<Spawner>().LostState = true;
        foreach (var obstacle in spawner.GetComponent<Spawner>().spawnedObstacles)
        {
            if (obstacle != null)
                obstacle.GetComponent<Mover>().LostState = true;
        }
    }

    private void EndPlayerChanges()
    {
        AudioManager.instance.Stop("rope");
        foreach (Animator arm in arms)
        {
            arm.speed = 0;
        }
    }

}
