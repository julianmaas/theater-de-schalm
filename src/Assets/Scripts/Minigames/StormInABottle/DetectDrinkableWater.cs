using System;
using UnityEngine;
using UnityEngine.Experimental.Rendering.Universal;

public class DetectDrinkableWater : MonoBehaviour
{
    private SpriteRenderer sr;
    private BoxCollider2D bc;
    public GameObject win;
    [SerializeField]
    
    public float timeToFind;
    private float timeOnBottle;
    public GameObject won;
    public new GameObject light;
    public GameObject cursorLight; 
    
    private void Awake()
    {
        sr = GetComponent<SpriteRenderer>();
        bc = GetComponent<BoxCollider2D>();
        timeOnBottle = 0;
    }

    private void Start()
    {
        AudioManager.instance.Play("rollinAt5");
    }

    private void OnCollisionStay2D(Collision2D other)
    {
        if (other.collider.gameObject.CompareTag("illucollider"))
        {
            timeOnBottle += 1 * Time.deltaTime;
            if (timeOnBottle >= timeToFind)
            {
                Win();
            }
        }
    }

    private void OnCollisionExit2D(Collision2D other)
    {
        if (other.collider.gameObject.CompareTag("illucollider"))
        {
            timeOnBottle = 0;
        }
    }

    private void Win()
    {
        AudioManager.instance.Stop("rollinAt5");
        won.GetComponent<WinGame>().Invoke();
        light.GetComponent<Light2D>().enabled = true;
        Destroy(cursorLight);
    }
}
