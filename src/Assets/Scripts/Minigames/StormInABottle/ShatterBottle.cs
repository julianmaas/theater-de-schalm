using System.Collections;
using UnityEngine;

public class ShatterBottle : MonoBehaviour
{

    private ParticleSystem particle;
    private SpriteRenderer sr;
    private BoxCollider2D bc;
    private AudioSource asource;
    private void Awake()
    {
        particle = GetComponentInChildren<ParticleSystem>();
        sr = GetComponent<SpriteRenderer>();
        bc = GetComponent<BoxCollider2D>();
        asource = GetComponent<AudioSource>();
    }

    private void OnCollisionEnter2D(Collision2D other)
    {
        var cooldown = 0;
        if (other.collider.gameObject.CompareTag("illucollider"))
        {
            //add 10% chance of destroying the bottle
            var chanceToDestroy = Random.Range(0, 100);
            if (chanceToDestroy <= 30 && cooldown <= 0)
            {
                StartCoroutine(Shatter());
                cooldown += 60;
            }

            cooldown--;
        }
    }

    private IEnumerator Shatter()
    {
        particle.Play();
        asource.Play();
        sr.enabled = false;
        bc.enabled = false;
        

        yield return new WaitForSeconds(particle.main.startLifetime.constantMax);
        Destroy(gameObject);
    }
}
