using UnityEngine;

public class AgentFollowCursor : MonoBehaviour
{
    public void Update()
    {
        Vector2 cursorPos = Camera.main.ScreenToWorldPoint(Input.mousePosition); //translate cursor to a position on the world
        transform.position = new Vector2(cursorPos.x, cursorPos.y); //update object position to match cursor position
    }
}
