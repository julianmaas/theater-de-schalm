﻿
using System;
using System.Linq;
using UnityEngine;
using Random = UnityEngine.Random;

public class HelmsmanRandomizer : MonoBehaviour
{
    public float boundaryWest;
    public float boundaryEast;
    public float boundarySouth; 
    public float boundaryNorth;

    public float minimumDistance;
    
    void Start()
    {
        foreach (Transform crewmate in transform)
        {
            if (crewmate.position.x >= 15.7f) continue; // Patchy stays where he is
            
            bool locationAccepted = false;
            byte count = byte.MinValue;
            while (!locationAccepted)
            {
                crewmate.position = 
                    new Vector2(Random.Range(boundaryWest, boundaryEast),
                        Random.Range(boundarySouth, boundaryNorth));

                // Make sure all crewmates are distant from each other
                locationAccepted = transform.Cast<Transform>()
                    .Where(otherCrewmate => otherCrewmate != crewmate)
                    .All(otherCrewmate => !Nearby(otherCrewmate.position, crewmate.position));
                
                // Lower minimum distance if script gets stuck
                count++; if (count == Byte.MaxValue) minimumDistance /= 1.1f;
            }
        }
    }
    
    private bool Nearby(Vector2 target, Vector2 current) => 
        Math.Abs(target.x - current.x) <= minimumDistance && Math.Abs(target.y - current.y) <= minimumDistance;
}