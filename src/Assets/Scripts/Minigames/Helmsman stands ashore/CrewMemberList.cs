using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class CrewMemberList : MonoBehaviour
{
    [Tooltip("Add valid helmsmen to the crew list.")]
    public List<Identification> helmsmen;

    void Start()
    {
        GetMembers();
    }
    
    public bool CheckHelmsman(Identification helmsman)
    {
        foreach (var item in helmsmen.Where(item => item == helmsman))
        {
            item.isChecked = true;
            helmsman.gameObject.SetActive(false);
            return true;
        }
        helmsman.gameObject.SetActive(false);
        return false;
    }

    public List<Identification> GetMembers()
    {
        return helmsmen;
    }

    public bool AllChecked()
    {
        return helmsmen.All(helmsman => helmsman.isChecked);
    }
}