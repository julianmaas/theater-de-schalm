﻿using UnityEngine;

public class RemoveConflictingProp : MonoBehaviour
{
    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.CompareTag("CrewMember")) Destroy(gameObject);
    }
}