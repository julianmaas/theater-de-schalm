﻿using UnityEngine;

public class HelmsmanDisableMovementOnWin : MonoBehaviour
{
    public HelmsmanGameManager game;

    private bool activated = false;
    
    public void Update()
    {
        if (activated) return;
        
        if (game.winState)
        {
            Destroy(gameObject.GetComponent<AgentInput>());
            var agentMovement = gameObject.GetComponent<AgentMovement>();
            agentMovement.currentVelocity = 0.0f;
            activated = true;
        }
    }
}