using UnityEngine;

public class Health : MonoBehaviour
{
    public int health;

    public int Hit()
    {
        return --health;
    }
}
