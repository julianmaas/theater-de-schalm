using UnityEngine;
using UnityEngine.Events;

public class CollisionDetector : MonoBehaviour
{
    public string tagToCheck;
    public UnityEvent onCollision;

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.transform.CompareTag(tagToCheck))
            onCollision.Invoke();
    }
}
