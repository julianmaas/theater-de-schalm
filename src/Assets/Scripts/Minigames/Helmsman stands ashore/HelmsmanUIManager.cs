using System.Collections.Generic;
using System.Linq;
using TMPro;
using UnityEngine;

public class HelmsmanUIManager : MonoBehaviour
{
    public GameObject crewMemberList;
    public List<TMP_Text> crewMemberListText;
    public TMP_Text fullname;
    public TMP_Text dayOfBirth;
    public TMP_Text homeTown;
    public TMP_Text married;
    public TMP_Text crewNumber;
    public TMP_Text health;
    public GameObject identification;
    public GameObject gameOver;

    public Animator[] healthOrbs;
    public string orbParamater;


    public void ShowCrewMemberList()
    {
        HideIdentification();
        var helmsmen = FindObjectOfType<CrewMemberList>().GetMembers();
        var zip = helmsmen.Zip(crewMemberListText, (h, t) => new {Helmsman = h, Text = t});
        foreach (var ht in zip)
        {
            if (ht.Helmsman.isChecked) ht.Text.color = Color.green;
            ht.Text.text =
                ht.Helmsman.fullname + " " +
                ht.Helmsman.homeTown + " " +
                ht.Helmsman.dayOfBirth + " " +
                TranslateMarriedToDutch(ht.Helmsman) + " " +
                ht.Helmsman.crewNumber;
        }
    }
    public void ShowIdentification(Identification id)
    {
        fullname.text = id.fullname;
        dayOfBirth.text = id.dayOfBirth;
        homeTown.text = id.homeTown;
        married.text = TranslateMarriedToDutch(id);
        crewNumber.text = id.crewNumber;
        identification.SetActive(true);
    }

    public void HideIdentification()
    {
        identification.SetActive(false);
    }

    public void SetHealth(int health)
    {
        if (health < 3)
        {
            healthOrbs[2 - health].SetTrigger(orbParamater);
        }
    }
    
    public void GameOver()
    {
        Time.timeScale = 0;
        gameOver.SetActive(true);
    }

    private string TranslateMarriedToDutch(Identification id)
    {
        return id.married ? "getrouwd" : "ongetrouwd";
    }
}