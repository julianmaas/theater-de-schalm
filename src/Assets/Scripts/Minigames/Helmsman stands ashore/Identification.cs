using UnityEngine;

public class Identification : MonoBehaviour
{
    public string fullname;
    public string dayOfBirth;
    public string homeTown;
    public string crewNumber;
    public bool married;
    public bool isChecked;
}