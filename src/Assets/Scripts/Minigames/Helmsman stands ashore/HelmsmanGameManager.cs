using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;

public class HelmsmanGameManager : MonoBehaviour
{
    public Identification closestCrewMember;
    public HelmsmanUIManager uiManager;
    public Health health;
    public GameObject won;
    public GameObject ui;
    
    internal bool winState = false;

    private void Start()
    {
        AudioManager.instance.Play("waves");
        AudioManager.instance.Play("seagull");
        AudioManager.instance.Play("delRioBravo");
        uiManager.SetHealth(health.health);
        ShowCrewMemberList();
    }

    public void ShowCrewMemberList()
    {
        uiManager.ShowCrewMemberList();
    }

    public void ShowIdentification(Identification id)
    {
        uiManager.ShowIdentification(id);
    }

    public void HideIdentification()
    {
        uiManager.HideIdentification();
    }

    public void Accept()
    {
        if (!FindObjectOfType<CrewMemberList>().CheckHelmsman(closestCrewMember))
        {
            uiManager.SetHealth(health.Hit());
            if (health.health == 0)
            {
                StartCoroutine(GoGameOver((float) 0.3));
            }
        }
        Won();
        HideIdentification();
    }

    private void GameOver()
    {
        if (health.health == 0) uiManager.GameOver();
    }

    public void Restart()
    {
        FindObjectOfType<AudioManager>().StopAll();
        Time.timeScale = 1;
        SceneManager.LoadScene("Helmsmen stands ashore");
    }
    
    private void Won()
    {
        if (!FindObjectOfType<CrewMemberList>().AllChecked()) return;
        winState = true;
        AudioManager.instance.Stop("delRioBravo");
        won.GetComponent<WinGame>().Invoke();
        Destroy(ui);
    }
    
    IEnumerator GoGameOver(float seconds)
    {
        yield return new WaitForSeconds(seconds);
        GameOver();
    }
}