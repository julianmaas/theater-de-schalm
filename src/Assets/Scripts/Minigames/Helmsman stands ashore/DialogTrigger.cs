﻿using UnityEngine;

public class DialogTrigger : MonoBehaviour
{
    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            Identification id = GetComponent<Identification>();
            FindObjectOfType<HelmsmanGameManager>().ShowIdentification(id);
            FindObjectOfType<HelmsmanGameManager>().closestCrewMember = id;

        }
    }

    private void OnTriggerExit2D(Collider2D other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            FindObjectOfType<HelmsmanGameManager>().HideIdentification();
            FindObjectOfType<HelmsmanGameManager>().closestCrewMember = null;
        }
    }
}