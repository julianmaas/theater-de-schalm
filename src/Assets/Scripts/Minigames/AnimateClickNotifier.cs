﻿using UnityEngine;

public class AnimateClickNotifier : MonoBehaviour
{
    public Sprite normal, inverted;
    public float durationOfOneSpriteInSeconds = 1.0f;
    public float moveCursorDownAmount = 0.05f;
    public float expectedCoordinateX, expectedCoordinateY;
    public bool sceneBiggerThanOneScreen = false;
    
    private float passedTime = 0.0f;
    private bool nextSpriteIsInverted = false;
    private byte waitCounter = 0;
    
    void Update()
    {
        passedTime += Time.deltaTime;
        if (passedTime >= durationOfOneSpriteInSeconds)
        {
            if (WaitCounterCheck()) GetComponent<SpriteRenderer>().enabled = true;
            GetComponent<SpriteRenderer>().sprite = nextSpriteIsInverted ? inverted : normal;
            if (sceneBiggerThanOneScreen)
            {
                var parent = transform.parent.position;
                transform.position = nextSpriteIsInverted
                    ? new Vector2(parent.x + expectedCoordinateX, parent.y + expectedCoordinateY + moveCursorDownAmount)
                    : new Vector2(parent.x + expectedCoordinateX,
                        parent.y + expectedCoordinateY - moveCursorDownAmount);
            }
            else
                transform.position = nextSpriteIsInverted
                    ? new Vector2(expectedCoordinateX, expectedCoordinateY + moveCursorDownAmount)
                    : new Vector2(expectedCoordinateX, expectedCoordinateY - moveCursorDownAmount);
            passedTime = 0.0f;
            nextSpriteIsInverted = !nextSpriteIsInverted;
        }
    }

    internal void StartShowing()
    {
        transform.localScale = new Vector3(1.0f, 1.0f, 1.0f);
        passedTime = 1.0f;
    }

    private bool WaitCounterCheck()
    {
        switch (waitCounter)
        {
            case 2:
                return false;
            case 1:
                waitCounter = 2;
                return true;
            default:
                waitCounter++;
                return false;
        }
    }
}