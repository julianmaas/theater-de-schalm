using System;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.Events;

// TODO: Refactor this
/// <summary> Main controller for the changing course minigame </summary>
/// <remarks> This code is pretty much spaghetti since we were still learning Unity as we wrote this </remarks>
public class WheelController : MonoBehaviour 
{
    #region Variables

    public TextMeshProUGUI rotationText;
    public GameObject rotationArrow;
    public Sprite fixedWheel;
    public GameObject wheelObject;
    public UnityEvent eventToInvoke;
    public GameObject win;
    
    /// <summary> Amount of commands before old wheel gets removed </summary>
    public int deconstructAmount = 5;
    /// <summary> Amount of commands before new wheel gets added </summary>
    public int constructAmount = 5;
    public int minRotationAmount = 2;
    public int maxRotationAmount = 5;
    public float rotationSpeed = 250.0f;
    
    public bool WinState { get; set; } = false;
    
    private float rotationAccumulator = 0.0f;
    private int commandIndex = 0;
    private IList<WheelCommand> wheelCommands = new List<WheelCommand>();

    private bool rotationStarted = false;
    private bool rotationActive = false;
    private bool rotationEnded = false;
    private bool wheelFallingSoundPlayed = false;
    
    private bool playGetNewWheelAnimation = false;
    private bool getNewWheelSecondPart = false;
    
    private static Vector2 offScreen = new Vector2(0,-9);
    private static Vector2 onScreen = new Vector2(0, 1.7f);
    #endregion
    
    #region Unity events
    private void Start()
    {
        wheelCommands = GetCommands();
        AudioManager.instance.Play("waves");
        AudioManager.instance.Play("seagull");
        AudioManager.instance.Play("deuces");
    }
    
    private void Update()
    {
        if (WinState) return;
        HandleInput();
        HandleCommand();
        HandleSound();
    }
    #endregion
    
    #region Commands
    /// <summary> Handles showing commands and going to the next command </summary>
    private void HandleCommand()
    {
        WheelCommand command = wheelCommands[commandIndex];
        // Determines if the command is finished by checking if wheel was rotated enough 
        int rotationMultiplier = command.Starboard ? -1 : 1; 
        if (rotationAccumulator * rotationMultiplier >= command.Amount * 360)
        {
            commandIndex++;
            if (commandIndex == deconstructAmount) // Play wheel changing animation
            {
                playGetNewWheelAnimation = true;
            }
            if (commandIndex >= wheelCommands.Count) // Win minigame
            {
                rotationText.text = " ";
                WinState = true;
                AudioManager.instance.Stop("deuces");
                win.GetComponent<WinGame>().Invoke();
                return;
            } 
            else command = wheelCommands[commandIndex];
            rotationAccumulator = 0.0f;
        }
        GetNewWheel();
        // Show remaining spins left
        double turnsToMake = Math.Ceiling((int) ((command.Amount * 360) - (rotationAccumulator * rotationMultiplier)) / 360.0f);
        if (turnsToMake > command.Amount) turnsToMake = command.Amount;
        if (turnsToMake <= 0) turnsToMake = 1;
        rotationText.text = $"{turnsToMake}×";
        // Set rotation of arrow
        rotationArrow.transform.localScale = command.Starboard 
            ? new Vector3(1.0f, 1.0f, 1.0f) 
            : new Vector3(-1.0f, 1.0f, 1.0f);
    }

    /// <summary> Generate the commands the player must execute to win the minigame </summary>
    private IList<WheelCommand> GetCommands()
    {
        var random = new System.Random();
        bool starboard = random.NextDouble() >= 0.5; // Determine starting rotation (L or R)
        IList<WheelCommand> wheelCommands = new List<WheelCommand>();
        for (int i = 0; i < deconstructAmount + constructAmount; i++)
        {
            wheelCommands.Add(new WheelCommand(starboard, random.Next(minRotationAmount+1, maxRotationAmount+1)));
            starboard = !starboard;
        }
        return wheelCommands;
    }
    
    private struct WheelCommand
    {
        internal bool Starboard;
        internal int Amount;

        public WheelCommand(bool starboard, int amount)
        {
            Starboard = starboard;
            Amount = amount;
        }
    }
    #endregion
    
    #region Input
    private void HandleInput()
    {
        if (playGetNewWheelAnimation) return;
        HandleInputAsRotationOnWheel();
        HandleInputAsRotationState();
    }

    private void HandleInputAsRotationOnWheel()
    {
        if (LeftInput)
        {
            float rotation = rotationSpeed * Time.deltaTime;
            transform.Rotate(0,0,rotation);
            rotationAccumulator += rotation;
        }
        if (RightInput)
        {
            float rotation = -rotationSpeed * Time.deltaTime;
            transform.Rotate(0,0,rotation);
            rotationAccumulator += rotation;
        }
    }

    private void HandleInputAsRotationState()
    {
        if (!rotationStarted && !rotationActive && (LeftInput || RightInput)) { rotationStarted = true; rotationActive = true; return; }
        if (rotationStarted && rotationActive) { rotationStarted = false; return; }
        if (rotationActive && (!LeftInput && !RightInput)) { rotationEnded = true; rotationActive = false; return; }
        if (rotationEnded && (!LeftInput && !RightInput)) { rotationEnded = false; return; }
    }
    private bool LeftInput => Input.GetKey(KeyCode.A) || Input.GetKey(KeyCode.LeftArrow);
    private bool RightInput => Input.GetKey(KeyCode.D) || Input.GetKey(KeyCode.RightArrow);
    #endregion

    #region Sound
    private void HandleSound()
    {
        if (rotationEnded || WinState || playGetNewWheelAnimation) AudioManager.instance.Stop("wheel");
        else if (rotationStarted) AudioManager.instance.Play("wheel");
        if (playGetNewWheelAnimation && !wheelFallingSoundPlayed)
        {
            AudioManager.instance.Play("impact3"); 
            wheelFallingSoundPlayed = true;
        }
    }
    #endregion
    
    #region Animations
    private void GetNewWheel()
    {
        if (!playGetNewWheelAnimation) return;
        // Movement
        transform.position = Vector2.MoveTowards(transform.position, 
            getNewWheelSecondPart ? onScreen : offScreen,
            (getNewWheelSecondPart ? 10 : 40) * Time.deltaTime);
        // Rotation
        float rotation = rotationSpeed/2 * Time.deltaTime;
        transform.Rotate(0,0,getNewWheelSecondPart ? -rotation : rotation);
        // Flags
        if (!getNewWheelSecondPart && transform.position.y <= offScreen.y)
        {
            getNewWheelSecondPart = true;
            wheelObject.GetComponent<SpriteRenderer>().sprite = fixedWheel;
        }
        if (getNewWheelSecondPart && transform.position.y >= onScreen.y) playGetNewWheelAnimation = false;
    }
    #endregion
}
