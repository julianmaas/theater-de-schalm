using System;
using System.Collections.Generic;
using System.Linq;
using Unity.Mathematics;
using UnityEngine;
using Random = UnityEngine.Random;

public class RebusGenerator : MonoBehaviour
{
    public float startPositionX = -7.0f;
    public float xGap = 3.85f;
    public float positionY = 4.0f;
    public GameObject one, two, three, four, five, six, seven, eight, nine;
    
    // Spawn rebus objects
    void Start()
    {
        AudioManager.instance.Play("treasureAmbience");
        var lockController = GetComponent<LockController>();
        var numberBag = new NumberBag(9);
        float positionX = startPositionX;
        
        lockController.first = numberBag.GetNumber(); 
        Instantiate(GetRebus(lockController.first), new Vector3(positionX, positionY), quaternion.identity);
        positionX += xGap;
        
        lockController.second = numberBag.GetNumber();
        Instantiate(GetRebus(lockController.second), new Vector3(positionX, positionY), quaternion.identity);
        positionX += xGap;
        
        lockController.third = numberBag.GetNumber();
        Instantiate(GetRebus(lockController.third), new Vector3(positionX, positionY), quaternion.identity);
        positionX += xGap;
        
        lockController.fourth = numberBag.GetNumber();
        Instantiate(GetRebus(lockController.fourth), new Vector3(positionX, positionY), quaternion.identity);
    }

    /// <summary> Convert number to rebus object </summary>
    private GameObject GetRebus(int input) =>
        input switch
        {
            1 => one, 2 => two, 3 => three,
            4 => four, 5 => five, 6 => six,
            7 => seven, 8 => eight, 9 => nine,
            _ => throw new ArgumentException("Invalid rebus number")
        };

    /// <summary> Data structure to make sure there is always a random number that isn't picked yet </summary>
    private class NumberBag
    {
        private IList<int> numbers = new List<int>();
        private int bagLimit;
        
        public NumberBag(int limit)
        {
            bagLimit = limit;
            FillBag();    
        }

        public int GetNumber()
        {
            if (!numbers.Any()) throw new ArgumentException("Number bag is empty, please refill");
            int number = numbers[Random.Range(1, numbers.Count)];
            numbers.Remove(number);
            return number;
        }

        public void FillBag()
        {
            for (int i = 1; i <= bagLimit; i++) numbers.Add(i);    
        }
    }
}
