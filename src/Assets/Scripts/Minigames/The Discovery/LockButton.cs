﻿using UnityEngine;

public class LockButton : MonoBehaviour
{
    public bool upButton;
    public GameObject lockNumber;

    void OnMouseDown()
    {
        AudioManager.instance.Play("lock");
        if (upButton) lockNumber.GetComponent<LockNumber>().Increase();
        else          lockNumber.GetComponent<LockNumber>().Decrease();
    }
    
}
