﻿using UnityEngine;

public class LockNumber : MonoBehaviour
{
    public Sprite one, two, three, four, five, six, seven, eight, nine;
    public int State { get; private set; } = 1;

    public void Increase()
    {
        State++;
        if (State == 10) State = 1;
        UpdateSprite();
    }

    public void Decrease()
    {
        State--;
        if (State == 0) State = 9;
        UpdateSprite();
    }

    private void UpdateSprite()
    {
        switch (State)
        {
            case 1: GetComponent<SpriteRenderer>().sprite = one; return;
            case 2: GetComponent<SpriteRenderer>().sprite = two; return;
            case 3: GetComponent<SpriteRenderer>().sprite = three; return;
            case 4: GetComponent<SpriteRenderer>().sprite = four; return;
            case 5: GetComponent<SpriteRenderer>().sprite = five; return;
            case 6: GetComponent<SpriteRenderer>().sprite = six; return;
            case 7: GetComponent<SpriteRenderer>().sprite = seven; return;
            case 8: GetComponent<SpriteRenderer>().sprite = eight; return;
            case 9: GetComponent<SpriteRenderer>().sprite = nine; return;
        }
    }
}