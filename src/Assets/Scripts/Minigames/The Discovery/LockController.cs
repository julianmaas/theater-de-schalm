﻿using UnityEngine;

public class LockController : MonoBehaviour
{
    public int first, second, third, fourth; // Combination of numbers to unlock the chest
    public GameObject firstLock, secondLock, thirdLock, fourthLock;
    public GameObject chest; public Sprite chestOpen;
    public GameObject won; public GameObject invalidCombination;
    public GameObject openButton;

    private bool unlockedInternal;
    
    public void Unlock()
    {
        if (unlockedInternal) return;
        
        if (Unlocked)
        {
            openButton.SetActive(false);
            unlockedInternal = true;
            chest.transform.position = chest.transform.position + new Vector3(0, 1, 0);
            chest.GetComponent<SpriteRenderer>().sprite = chestOpen;
            won.GetComponent<WinGame>().Invoke();
            AudioManager.instance.Stop("treasureAmbience");
            gameObject.SetActive(false);
        }
        else
        {
            invalidCombination.transform.localScale = new Vector2(1.0f, 1.0f);
        }
    }
    public bool Unlocked =>
        firstLock.GetComponent<LockNumber>().State == first &&
        secondLock.GetComponent<LockNumber>().State == second &&
        thirdLock.GetComponent<LockNumber>().State == third &&
        fourthLock.GetComponent<LockNumber>().State == fourth;
}