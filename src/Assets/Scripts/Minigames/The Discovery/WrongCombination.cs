﻿using UnityEngine;

public class WrongCombination : MonoBehaviour
{
    public float transitionDuration = 7.0f;
    public float scale = 1.0f;
    
    private float TransitionAmountPerSecond => scale / transitionDuration;
    
    void Update()
    {
        transform.localScale = transform.localScale.x > 0 ? CalculateNewScale() : new Vector2(0.0f, 0.0f);
    }
    
    private Vector2 CalculateNewScale()
    {
        float scale = transform.localScale.x - (Time.deltaTime * TransitionAmountPerSecond);
        return new Vector2(scale, scale);
    }
}
