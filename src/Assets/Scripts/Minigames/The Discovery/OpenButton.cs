﻿using UnityEngine;

public class OpenButton : MonoBehaviour
{
    public GameObject Lock;
    
    void OnMouseDown()
    {
        AudioManager.instance.Play("unlock");
        Lock.GetComponent<LockController>().Unlock();    
    }

    void ShakeButton()
    {
        
    }
}