using UnityEngine;
using UnityEngine.UI;

public class ButtonController : MonoBehaviour
{
    public Sprite[] buttonSprites;
    private int index = 0;
    
    public void ChangeSprite()
    {
        index = (index - 1) * -1;
        GetComponent<Image>().sprite = buttonSprites[index];
    }
}
