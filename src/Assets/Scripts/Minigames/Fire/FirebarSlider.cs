using UnityEngine;
using UnityEngine.UI;

public class FirebarSlider : MonoBehaviour
{

    public Slider slider;
    public Image filler;
    public Image fireImage;

    public void SetMaxFireHealth(float maxHealth)
    {
        slider.maxValue = maxHealth;
    }
    
    public void SetFireHealth(float health)
    {
        slider.value = health;
    }

    public void SetFillerColor(Color color)
    {
        filler.color = color;
    }

    public void SetFireSize(Vector3 size)
    {
        fireImage.transform.localScale = size;
    }
}
