using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Experimental.Rendering.Universal;
using UnityEngine.UI;

public class FireGameManager : MonoBehaviour
{
    #region Variables

    public GameObject[] fires;
    public GameObject[] fireLights;
    public FirebarSlider firebar;
    
    public GameObject won;
    public GameObject wonLight;
    public GameObject explanationMark;
    
    public bool Won { get; set; } = false;
    public bool LostState { get; set; } = false;
    
    
    public float scale = 0.8f;
    public float scaleModifier = 0.0002f;
    public float scaleLoseCondition;
    
    #region Input Variables
    private bool UpInput => Input.GetKey(KeyCode.W) || Input.GetKey(KeyCode.UpArrow);
    private bool DownInput => Input.GetKey(KeyCode.S) || Input.GetKey(KeyCode.DownArrow);
    private bool LeftInput => Input.GetKey(KeyCode.A) || Input.GetKey(KeyCode.LeftArrow);
    private bool RightInput => Input.GetKey(KeyCode.D) || Input.GetKey(KeyCode.RightArrow);

    private bool[] inputsToCheck;
    
    public GameObject[] buttons;
    
    private int currentFocusInput = 0;
    #endregion
    #endregion
    
    // Start is called before the first frame update
    void Start()
    {
        AudioManager.instance.Play("fire");
        AudioManager.instance.Play("troubleWithTribals");
        inputsToCheck = new bool[4];
        
        InvokeRepeating("ChangeFocus", 1, 4); // function string, start after float, repeat rate float
        
        firebar.SetMaxFireHealth(scaleLoseCondition);
        firebar.SetFireHealth(scale);
    }
    
    public void Update()
    {
        if (LostState) return;
        if (Won) return;
        firebar.SetFireHealth(scale);
        HandleInput();
        ScaleCalculations();
    }
    
    /// <summary> Changes the focus button </summary>
    void ChangeFocus ()
    {
        List<int> validChoices = new List<int>();
        for (int i = 0; i < inputsToCheck.Length; i++)
        {
            if(i != currentFocusInput)
                validChoices.Add(i);
        }
        currentFocusInput = validChoices[Random.Range(0, validChoices.Count)];
    }
    
    #region Input
    
    private void HandleInput()
    {
        inputsToCheck[0] = UpInput;
        inputsToCheck[1] = DownInput;
        inputsToCheck[2] = LeftInput;
        inputsToCheck[3] = RightInput;
    }

    #region Logic
    /// <summary> Change the size of the fire based on microphone loudness </summary>
    private void ScaleCalculations()
    {
        int correctInputCount = -6;
        for (int i = 0; i < inputsToCheck.Length; i++)
        {
            Animator buttonAnimator = buttons[i].GetComponent<Animator>();
            if (inputsToCheck[i])
            {
                if (i == currentFocusInput)
                {
                    buttons[i].GetComponent<Image>().color = Color.green;
                    correctInputCount += 12;
                }
                else
                {
                    buttons[i].GetComponent<Image>().color = Color.red;
                    correctInputCount -= 6;
                }
                buttonAnimator.SetBool("isPressed", true);
            }
            else
            {
                buttonAnimator.SetBool("isPressed", false);
                buttons[i].GetComponent<Image>().color = Color.white;
            }
        }

        ChangeBlowingOut(correctInputCount);
        ChangeFirebar(correctInputCount);
        
        scale -= scaleModifier * correctInputCount * Time.deltaTime;
        
        CheckScale();
        
        ChangeFires(new Vector3(scale, scale, 1.0f));
        
    }

    private void CheckScale()
    {
        if (scale <= 0.0f) // Win minigame
        {
            Win();
        }

        if (scale >= scaleLoseCondition)
        {
            GameOver();
        }
    }

    /// <summary> Change the size of all fires </summary>
    private void ChangeFires(Vector3 newVector)
    {
        foreach (GameObject fire in fires)
        {
            fire.transform.localScale = newVector;
        }
    }
    
    private void ChangeBlowingOut(int count)
    {
        if (count > 0)
        {
            if (!AudioManager.instance.GetSound("blowing").source.isPlaying)
                AudioManager.instance.Play("blowing");
        }
        else
        {
            
            AudioManager.instance.Stop("blowing");
        }
    }

    Color newGreen = new Color(0.1520859f, 0.8207547f, 0.05032932f);
    Color newOrange = new Color(0.9622642f, 0.2928624f, 0.1497864f);
    Color newRed = new Color(0.9622642f, 0.2928624f, 0.1497864f);

    public void ChangeFirebar(int count)
    {
        if (count > 0)
        {
            firebar.SetFillerColor(newGreen);
            firebar.SetFireSize(new Vector3(0.5f, 0.5f, 1.0f));
        }
        else if (count >= -6)
        {
            firebar.SetFillerColor(newOrange);
            firebar.SetFireSize(new Vector3(0.8f, 0.8f, 1.0f));
        }
        else
        {
            firebar.SetFillerColor(newRed);
            firebar.SetFireSize(new Vector3(1.1f, 1.1f, 1.0f));
        }
    }

    #endregion

    #region GameStates

    public GameObject uiToHide;

    private void Win()
    {
        Won = true;
        won.GetComponent<WinGame>().Invoke();
        wonLight.GetComponent<Light2D>().enabled = true;
        foreach (var fireLight in fireLights)
        {
            fireLight.GetComponent<Light2D>().enabled = false;
        }
        ChangeFires(new Vector3(0.0f, 0.0f, 0.0f));
        AudioManager.instance.Stop("fire");
        AudioManager.instance.Stop("blowing");
        AudioManager.instance.Stop("troubleWithTribals");
        Destroy(explanationMark);
        uiToHide.SetActive(false);
    }

    public GameObject gameOverButton;
    
    private void GameOver()
    {
        LostState = true;
        gameOverButton.SetActive(true);
        scaleModifier = 0;
    }

    #endregion
    
    #endregion
}
