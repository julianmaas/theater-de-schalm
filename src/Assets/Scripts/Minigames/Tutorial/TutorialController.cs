using UnityEngine;
using UnityEngine.UI;

public class TutorialController : MonoBehaviour
{
    public Sprite[] tutorialImages;
    public GameObject imageToRender;
    public string[] tutorialTexts;
    public GameObject textToRender;
    private int index = 0;
    
    public GameObject leftButton;
    public GameObject rightButton;
    public GameObject startButton;
    
    // Start is called before the first frame update
    void Start()
    {
        index = 0;
        ChangeImage();
        CheckButtons();
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.A) || Input.GetKeyDown(KeyCode.LeftArrow))
            IncrementIndex(-1);
        if (Input.GetKeyDown(KeyCode.D) || Input.GetKeyDown(KeyCode.RightArrow))
            IncrementIndex(1);
    }

    public void IncrementIndex(int addition)
    {
        index += addition;
        if (index <= 0)
            index = 0;
        if (index >= tutorialImages.Length)
        {
            GetComponent<SceneTransition>().ChangeScene();
            return;
        }
        ChangeImage();
        CheckButtons();
    }

    private void ChangeImage()
    {
        imageToRender.GetComponent<Image>().sprite = tutorialImages[index];
        textToRender.GetComponent<Text>().text = tutorialTexts[index];
    }

    private void CheckButtons()
    {
        leftButton.SetActive(index != 0);

        rightButton.SetActive(index != tutorialImages.Length - 1);
        
        startButton.SetActive(index == tutorialImages.Length - 1);
    }
}
